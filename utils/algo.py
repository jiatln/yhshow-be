from functools import reduce
import itertools
import sys

from utils import data_format


class YuhunComb(object):
    def __init__(self, yuhun_list, l2_prop_limit, l4_prop_limit, l6_prop_limit, optimize_pane, limit_props, limit_pane, plan, shishen_pane):
        """Calculate total damage
        Args:
            yuhun_list (list): 御魂列表
            l2_prop_limit (list): 二号位主属性
            optimize_pane (str): 优化目标
            limit_props (dict): 属性限制 如： {'速度': {'min': 0, 'max': 128 } }
            limit_pane (dict): 面板限制 如： {'输出伤害': {'min': 17800, 'max': None } }
            effective_props (list): 有效属性, 计算要用到的
            plan (dict): 御魂套装方案 {'4':'狂骨', '2':'荒骷髅'}
            shishen_pane (dict): 式神面板
        """
        self.yuhun_dict = self.yuhun_list2dict(yuhun_list)
        self.l2_prop_limit = l2_prop_limit
        self.l4_prop_limit = l4_prop_limit
        self.l6_prop_limit = l6_prop_limit
        self.optimize_pane = optimize_pane
        self.limit_props = limit_props
        self.effective_props = list(set(data_format.MITAMA_PROPS_EFFECT[optimize_pane]) | set(limit_props.keys()))
        print(self.effective_props)
        self.plan = plan
        self.shishen_pane = shishen_pane
        self.calculated_count = 0
        self.printed_rate = 0
        self.total_comb = 0

    def yuhun_list2dict(self, yuhun_list):
        yuhun_dict = {1: [], 2: [], 3: [], 4: [], 5: [], 6: []}
        for yuhun in yuhun_list:
            yuhun = self.format_yyx(yuhun)
            yuhun_dict[yuhun['loc']].append(yuhun)
        return yuhun_dict

    @staticmethod
    def format_yyx(yuhun):
        # loc 位置
        # star 星级
        # level 等级
        # id id
        # name 破势
        # base_attr 主属性
        # type 二件套加成
        new_yuhun = {}
        new_yuhun['loc'] = yuhun['pos'] + 1
        new_yuhun['star'] = yuhun['quality']
        new_yuhun['level'] = yuhun['level']
        new_yuhun['id'] = yuhun['id']
        new_yuhun['name'] = data_format.get_name_by_suit_id(yuhun['suit_id'])
        new_yuhun['base_attr'] = yuhun['base_attr']['type']
        new_yuhun['base_value'] = yuhun['base_attr']['value']
        new_yuhun['single_attr'] = '' if yuhun['single_attrs'] == [] else yuhun['single_attrs'][0]['type']
        new_yuhun['single_value'] = 0 if yuhun['single_attrs'] == [] else yuhun['single_attrs'][0]['value']
        new_yuhun['attrs'] = {item['type']: item['value'] for item in yuhun['attrs']}
        new_yuhun['type'] = data_format.MITAMA_ENHANCE[new_yuhun['name']]['加成类型']
        return new_yuhun

    def filter_loc_prop(self, yuhun_list, prop_limit):
        return list(filter(lambda x: x['base_attr'] in prop_limit, yuhun_list))

    def clean(self):
        # yuhun_count = reduce(lambda x, y: x*y, map(len, self.yuhun_dict.values()))
        print(list(map(len, self.yuhun_dict.values())))
        # 过滤低星及未满级御魂
        for i in range(1, 7):
            self.yuhun_dict[i] = list(filter(lambda x: x['star'] == 6 and x['level'] == 15, self.yuhun_dict[i]))
        # 过滤二四六号位的御魂
        if self.l2_prop_limit:
            self.yuhun_dict[2] = self.filter_loc_prop(self.yuhun_dict[2], self.l2_prop_limit)
        if self.l4_prop_limit:
            self.yuhun_dict[4] = self.filter_loc_prop(self.yuhun_dict[4], self.l4_prop_limit)
        if self.l6_prop_limit:
            self.yuhun_dict[6] = self.filter_loc_prop(self.yuhun_dict[6], self.l6_prop_limit)
        # 过滤不包含有效属性的御魂
        for i in range(1, 7):
            self.yuhun_dict[i] = list(filter(lambda x:
                                             set(x['attrs'].keys()) & set(self.effective_props), self.yuhun_dict[i]))
        for i in range(1, 7):
            self.yuhun_dict[i] = self.filter_lower_yuhun(self.yuhun_dict[i])
        # yuhun_count = reduce(lambda x, y: x*y, map(len, self.yuhun_dict.values()))
        print(list(map(len, self.yuhun_dict.values())))

        # 处理self.limit_props
        if 'Speed' in self.limit_props.keys():
            if self.limit_props['Speed']['min'] is not None:
                self.limit_props['Speed']['min'] -= self.shishen_pane['速度']
            if self.limit_props['Speed']['max'] is not None:
                self.limit_props['Speed']['max'] -= self.shishen_pane['速度']
        if 'CritRate' in self.limit_props.keys():
            if self.limit_props['CritRate']['min'] is not None:
                self.limit_props['CritRate']['min'] -= self.shishen_pane['暴击']
            if self.limit_props['CritRate']['max'] is not None:
                self.limit_props['CritRate']['max'] -= self.shishen_pane['暴击']
        if 'CritPower' in self.limit_props.keys():
            if self.limit_props['CritPower']['min'] is not None:
                self.limit_props['CritPower']['min'] -= self.shishen_pane['暴击伤害']
            if self.limit_props['CritPower']['max'] is not None:
                self.limit_props['CritPower']['max'] -= self.shishen_pane['暴击伤害']

    def get_yuhun_combos(self):

        def find_candiadates(yuhun='_'):
            print(yuhun)
            # 寻找候选御魂
            candidates = []
            if yuhun == '_' or yuhun == 0 or yuhun == None:
                candidates = data_format.MITAMA_NAMES
            elif yuhun in data_format.MITAMA_NAMES:
                candidates.append(yuhun)
            elif yuhun in data_format.MITAMA_PROPS:
                for name in data_format.MITAMA_NAMES:
                    if data_format.MITAMA_ENHANCE[name]['加成类型'] == yuhun:
                        candidates.append(name)
            return candidates

        # 获取御魂组合
        main_candiadates = []
        secondary_candiadates = []
        if len(self.plan) == 2:
            # 4 + 2 的情况
            main_candiadates = find_candiadates(self.plan['4'])
            secondary_candiadates = find_candiadates(self.plan['2'])
            for four in main_candiadates:
                for two in secondary_candiadates:
                    yield [four, four, four, four, two, two]

    def get_yuhun_permutations(self):
        # 生成御魂排列
        combo_list = self.get_yuhun_combos()
        for combo in combo_list:
            for perm in set(itertools.permutations(combo)):
                yield perm

    def make_combocation(self):
        # 生成组合
        yuhun_permutations = self.get_yuhun_permutations()
        product_list = []
        for perm in yuhun_permutations:
            comb_yuhun_list = [[], [], [], [], [], []]
            for index, name in enumerate(perm):
                comb_yuhun_list[index] = [y for y in self.yuhun_dict[index + 1] if y['name'] == name]
            cur_combo_num = reduce(lambda x, y: x * y, map(len, comb_yuhun_list))
            if cur_combo_num:
                self.total_comb += cur_combo_num
                product_list.append(itertools.product(*comb_yuhun_list))
        print('组合数：', self.total_comb)
        return itertools.chain(*product_list)

    def pipeline(self):
        self.clean()
        combo_list = self.make_combocation()
        combo_list = map(lambda x: self.add_yuhun_name(x), combo_list)
        combo_list = map(lambda x: self.add_sum_props(x), combo_list)
        # combo = combo_list.__next__()
        # print(combo)
        for prop in self.limit_props:
            combo_list = self.filter_prop_limit(combo_list, prop)
        # print(self.calc_pane(combo))
        combo_list = map(lambda x: self.calc_pane(x), combo_list)
        # 求最大值
        res = max(combo_list, key=lambda dic: dic['optimize_pane'], default={})
        return res

    def add_yuhun_name(self, combo):
        yuhun_name_count = {}
        for yuhun in combo:
            if yuhun['name'] in yuhun_name_count:
                yuhun_name_count[yuhun['name']] += 1
            else:
                yuhun_name_count[yuhun['name']] = 1

        return {
            'summary': yuhun_name_count,
            'info': list(combo)
        }

    def add_sum_props(self, combo):
        # 六个位置及套装属性求和
        yuhun_pane = dict(zip(self.effective_props, [0]*len(self.effective_props)))
        for yuhun in combo['info']:
            # 算上基础属性
            if yuhun['base_attr'] in self.effective_props:
                yuhun_pane[yuhun['base_attr']] += yuhun['base_value']
            # 算上首领御魂单属性
            if yuhun['single_attr'] in self.effective_props:
                yuhun_pane[yuhun['single_attr']] += yuhun['single_value']
            for (k, v) in yuhun['attrs'].items():
                if k in self.effective_props:
                    yuhun_pane[k] += v

        # 算上御魂效果的加成 有效属性
        for k, v in combo['summary'].items():
            if v < 2:
                continue
            prop_type = data_format.MITAMA_ENHANCE[k]['加成类型']
            if prop_type in self.effective_props:
                yuhun_pane[prop_type] += data_format.MITAMA_ENHANCE[k]['加成数值']
        combo['yuhun_pane'] = yuhun_pane
        # combo['info'] = list(map(lambda x: x['id'], combo['info']))
        return combo

    def calc_pane(self, combo):
        pane = 0
        ss_pane = self.shishen_pane
        yh_pane = combo['yuhun_pane']
        if self.optimize_pane == '输出伤害':
            # 输出伤害 = ((式神基础攻击 * (1 + 攻击加成)) + 小攻击) * (基础暴伤 + 御魂暴伤)
            pane = ((ss_pane['攻击'] * (1 + yh_pane['AttackRate'])) +
                    yh_pane['Attack']) * (ss_pane['暴击伤害'] + yh_pane['CritPower'])

        if self.optimize_pane == '双重暴击':
            # 双重暴击 = ((式神基础攻击 * (1 + 攻击加成)) + 小攻击) * (基础暴伤 + 御魂暴伤) ^ 2
            pane = ((ss_pane['攻击'] * (1 + yh_pane['AttackRate'])) +
                    yh_pane['Attack']) * pow(ss_pane['暴击伤害'] + yh_pane['CritPower'], 2)

        if self.optimize_pane == '生命治疗':
            # 生命治疗 = ((式神基础生命 * (1 + 生命加成)) + 小生命) * (基础暴伤 + 御魂暴伤)
            pane = ((ss_pane['生命'] * (1 + yh_pane['HpRate'])) +
                    yh_pane['Hp']) * (ss_pane['暴击伤害'] + yh_pane['CritPower'])

        if self.optimize_pane == '命抗双修':
            pane = ss_pane['效果命中'] + ss_pane['效果抵抗'] + \
                yh_pane['EffectHitRate'] + yh_pane['EffectResistRate']

        if self.optimize_pane == '速度':
            pane = ss_pane['速度'] + yh_pane['Speed']

        combo['optimize_pane'] = pane
        return combo

    def filter_prop_limit(self, combo_list, prop_name):
        prop_min = self.limit_props[prop_name]['min'] or 0
        prop_max = self.limit_props[prop_name]['max'] or float('inf')
        return filter(lambda x: prop_min <= x['yuhun_pane'].get(prop_name, 0) <= prop_max, combo_list)

    def filter_lower_yuhun(self, yuhun_list=[]):
        # 过滤御魂属性较弱的御魂 不能比较[非自由]属性
        # 比如 一号位 3攻击 8暴击 就不如一个 5攻击 8暴击的 此时把前者过滤掉
        # [非自由]属性 定义:如果速度有上限 那3速度和5速度相比 都有可能是最佳属性 此时不能过滤掉前者
        not_free_prop_list = [k for k, v in self.limit_props.items() if v.get('max', 0) is not None]
        # not_free_prop_list = [k for k, v in self.limit_props.items() if v.get('max', 0) < float('inf')]
        need_remove_list = []
        for yh_a in yuhun_list:
            if set(yh_a['attrs'].keys()) & set(not_free_prop_list):
                continue
            # 御魂a 与御魂list进行比较, 如果御魂list中存在完胜御魂a的 则将a剔除掉
            for yh_b in yuhun_list:
                if yh_a['id'] in need_remove_list:
                    break
                if set(yh_a['attrs'].keys()) & set(not_free_prop_list):
                    continue
                if yh_a['id'] == yh_b['id'] or yh_a['name'] != yh_b['name'] or yh_a['base_attr'] != yh_b['base_attr']:
                    continue

                a_better_than_b = False
                for porp in self.effective_props:
                    yh_a_porp = yh_a['attrs'].get(porp, 0)
                    yh_b_porp = yh_b['attrs'].get(porp, 0)
                    if yh_a_porp > yh_b_porp:
                        a_better_than_b = True
                        break
                if not a_better_than_b:
                    need_remove_list.append(yh_a['id'])
                    break
        good_list = [yuhun for yuhun in yuhun_list if yuhun['id'] not in need_remove_list]
        print(f'已过滤御魂{len(need_remove_list)}个')
        return good_list
