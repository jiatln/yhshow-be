MITAMA_PROPS_EFFECT = {
    '输出伤害': ['AttackRate', 'Attack', 'CritPower'],
    '双重暴击': ['AttackRate', 'Attack', 'CritPower'],
    '生命治疗': ['HpRate', 'CritPower'],
    '命抗双修': ['EffectHitRate', 'EffectResistRate'],
    '攻击': ['AttackRate', 'Attack'],
    '生命': ['HpRate', 'Hp'],
    '防御': ['DefenseRate', 'Defense'],
    '暴击伤害': ['CritPower'],
    '暴击': ['CritRate'],
    '速度': ['Speed'],
    '效果命中': ['EffectHitRate'],
    '效果抵抗': ['EffectResistRate'],
}
MITAMA_ENHANCE = {
    "珍珠": {"加成类型": "DefenseRate", "加成数值": 0.3},
    "骰子鬼": {"加成类型": "EffectResistRate", "加成数值": 0.15},
    "蚌精": {"加成类型": "EffectHitRate", "加成数值": 0.15},
    "魅妖": {"加成类型": "DefenseRate", "加成数值": 0.3},
    "针女": {"加成类型": "CritRate", "加成数值": 0.15},
    "返魂香": {"加成类型": "EffectResistRate", "加成数值": 0.15},
    "雪幽魂": {"加成类型": "DefenseRate", "加成数值": 0.3},
    "地藏像": {"加成类型": "HpRate", "加成数值": 0.15},
    "蝠翼": {"加成类型": "AttackRate", "加成数值": 0.15},
    "涅槃之火": {"加成类型": "HpRate", "加成数值": 0.15},
    "三味": {"加成类型": "CritRate", "加成数值": 0.15},
    "魍魉之匣": {"加成类型": "EffectResistRate", "加成数值": 0.15},
    "被服": {"加成类型": "HpRate", "加成数值": 0.15},
    "招财猫": {"加成类型": "DefenseRate", "加成数值": 0.3},
    "反枕": {"加成类型": "DefenseRate", "加成数值": 0.3},
    "轮入道": {"加成类型": "AttackRate", "加成数值": 0.15},
    "日女巳时": {"加成类型": "DefenseRate", "加成数值": 0.3},
    "镜姬": {"加成类型": "HpRate", "加成数值": 0.15},
    "钟灵": {"加成类型": "HpRate", "加成数值": 0.15},
    "狰": {"加成类型": "AttackRate", "加成数值": 0.15},
    "火灵": {"加成类型": "EffectHitRate", "加成数值": 0.15},
    "鸣屋": {"加成类型": "AttackRate", "加成数值": 0.15},
    "薙魂": {"加成类型": "HpRate", "加成数值": 0.15},
    "心眼": {"加成类型": "AttackRate", "加成数值": 0.15},
    "木魅": {"加成类型": "DefenseRate", "加成数值": 0.3},
    "树妖": {"加成类型": "HpRate", "加成数值": 0.15},
    "网切": {"加成类型": "CritRate", "加成数值": 0.15},
    "阴摩罗": {"加成类型": "AttackRate", "加成数值": 0.15},
    "伤魂鸟": {"加成类型": 'CritRate', "加成数值": 0.15},
    "破势": {"加成类型": 'CritRate', "加成数值": 0.15},
    "镇墓兽": {"加成类型": 'CritRate', "加成数值": 0.15},
    "狂骨": {"加成类型": "AttackRate", "加成数值": 0.15},
    "幽谷响": {"加成类型": "EffectResistRate", "加成数值": 0.15},
    "兵主部": {"加成类型": "AttackRate", "加成数值": 0.15},
    "青女房": {"加成类型": 'CritRate', "加成数值": 0.15},
    "涂佛": {"加成类型": "HpRate", "加成数值": 0.15},
    "飞缘魔": {"加成类型": "EffectHitRate", "加成数值": 0.15},
    "土蜘蛛": {"加成类型": "首领御魂", "加成数值": 0},
    "胧车": {"加成类型": "首领御魂", "加成数值": 0},
    "荒骷髅": {"加成类型": "首领御魂", "加成数值": 0},
    "地震鲶": {"加成类型": "首领御魂", "加成数值": 0},
    "蜃气楼": {"加成类型": "首领御魂", "加成数值": 0},
    "鬼灵歌伎": {"加成类型": "首领御魂", "加成数值": 0}
}


MITAMA_NAMES = list(MITAMA_ENHANCE.keys())


MITAMA_PROPS = [
    'AttackRate',
    'DefenseRate',
    'CritRate',
    'HpRate',
    'EffectHitRate',
    'EffectResistRate',
    '首领御魂'
]

MITAMA_ID_LIST = [
    {
        'name': '招财猫',
        'id': 300010,
        'type': 'DefenseRate',
    },
    {
        'name': '破势',
        'id': 300030,
        'type': 'CritRate',

    },
    {
        'name': '针女',
        'id': 300036,
        'type': 'CritRate',

    },
    {
        'name': '薙魂',
        'id': 300021,
        'type': 'HpRate',

    },
    {
        'name': '雪幽魂',
        'id': 300002,
        'type': 'DefenseRate',
    },
    {
        'name': '魅妖',
        'id': 300035,
        'type': 'DefenseRate',
    },
    {
        'name': '三味',
        'id': 300007,
        'type': 'CritRate',
    },
    {
        'name': '狂骨',
        'id': 300048,
        'type': 'AttackRate',
    },
    {
        'name': '树妖',
        'id': 300024,
        'type': 'HpRate',
    },
    {
        'name': '涅槃之火',
        'id': 300006,
        'type': 'HpRate',
    },
    {
        'name': '网切',
        'id': 300026,
        'type': 'CritRate',
    },
    {
        'name': '反枕',
        'id': 300011,
        'type': 'DefenseRate',
    },
    {
        'name': '日女巳时',
        'id': 300013,
        'type': 'DefenseRate',
    },
    {
        'name': '珍珠',
        'id': 300032,
        'type': 'DefenseRate',
    },
    {
        'name': '轮入道',
        'id': 300012,
        'type': 'AttackRate',
    },
    {
        'name': '蜃气楼',
        'id': 300054,
        'type': '首领御魂',
    },
    {
        'name': '地藏像',
        'id': 300003,
        'type': 'HpRate',
    },
    {
        'name': '心眼',
        'id': 300022,
        'type': 'AttackRate',
    },
    {
        'name': '鸣屋',
        'id': 300020,
        'type': 'AttackRate',
    },
    {
        'name': '狰',
        'id': 300018,
        'type': 'AttackRate',
    },
    {
        'name': '荒骷髅',
        'id': 300052,
        'type': '首领御魂',
    },
    {
        'name': '蚌精',
        'id': 300034,
        'type': 'EffectHitRate',
    },
    {
        'name': '阴摩罗',
        'id': 300027,
        'type': 'AttackRate',
    },
    {
        'name': '鬼灵歌伎',
        'id': 300077,
        'type': '首领御魂',
    },
    {
        'name': '钟灵',
        'id': 300015,
        'type': 'HpRate',
    },
    {
        'name': '伤魂鸟',
        'id': 300029,
        'type': 'CritRate',
    },
    {
        'name': '木魅',
        'id': 300023,
        'type': 'DefenseRate',
    },
    {
        'name': '火灵',
        'id': 300019,
        'type': 'EffectHitRate',
    },
    {
        'name': '土蜘蛛',
        'id': 300050,
        'type': '首领御魂',
    },
    {
        'name': '镇墓兽',
        'id': 300031,
        'type': 'CritRate',
    },
    {
        'name': '兵主部',
        'id': 300074,
        'type': 'AttackRate',
    },
    {
        'name': '镜姬',
        'id': 300014,
        'type': 'HpRate',
    },
    {
        'name': '地震鲶',
        'id': 300053,
        'type': '首领御魂',
    },
    {
        'name': '魍魉之匣',
        'id': 300008,
        'type': 'EffectResistRate',
    },
    {
        'name': '蝠翼',
        'id': 300004,
        'type': 'AttackRate',
    },
    {
        'name': '幽谷响',
        'id': 300049,
        'type': 'EffectResistRate',
    },
    {
        'name': '胧车',
        'id': 300051,
        'type': '首领御魂',
    },
    {
        'name': '返魂香',
        'id': 300039,
        'type': 'EffectResistRate',
    },
    {
        'name': '青女房',
        'id': 300075,
        'type': 'CritRate',
    },
    {
        'name': '骰子鬼',
        'id': 300033,
        'type': 'EffectResistRate',
    },
    {
        'name': '飞缘魔',
        'id': 300073,
        'type': 'EffectHitRate',
    },
    {
        'name': '被服',
        'id': 300009,
        'type': 'HpRate',
    },
    {
        'name': '涂佛',
        'id': 300076,
        'type': 'HpRate',
    },
]


def get_name_by_suit_id(suit_id):
    item = list(filter(lambda item: item['id'] == suit_id, MITAMA_ID_LIST))
    return item[0]['name']
