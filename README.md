# yhshow-be

御魂计算器后端

## 创建虚拟环境

`python -m venv venv`

## 激活虚拟环境

`source venv/Scripts/activate`

## 安装依赖

`pip install -r requirements.txt`

## 启动项目

`./run.sh`
